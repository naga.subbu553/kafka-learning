package com.example.kafkaconsumerlibrary.entity;

public enum LibraryEventType {
    NEW,
    UPDATE
}
