package com.example.kafkaconsumerlibrary.jpa;

import com.example.kafkaconsumerlibrary.entity.LibraryEvent;
import org.springframework.data.repository.CrudRepository;

public interface LibraryEventsRepository extends CrudRepository<LibraryEvent,Integer> {
}
