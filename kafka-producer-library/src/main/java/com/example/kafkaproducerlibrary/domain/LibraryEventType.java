package com.example.kafkaproducerlibrary.domain;

public enum LibraryEventType {
    NEW,
    UPDATE
}
