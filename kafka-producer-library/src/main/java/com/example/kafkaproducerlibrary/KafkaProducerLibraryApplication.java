package com.example.kafkaproducerlibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaProducerLibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaProducerLibraryApplication.class, args);
	}

}
